package com.example.coursera.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "instructor")
public class Student extends Human {

    @Id
    private String pin;

    @ManyToMany
    private List<Course> studentCourses;

    public List<Course> getStudentCourses() {
        return studentCourses;
    }

    public void setStudentCourses(List<Course> studentCourses) {
        this.studentCourses = studentCourses;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
