package com.example.coursera.controller.rest;

import com.example.coursera.services.contracts.CourseService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }
}
