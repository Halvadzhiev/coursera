package com.example.coursera.controller.rest;

import com.example.coursera.services.contracts.InstructorService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InstructorController {

    private final InstructorService instructorService;

    public InstructorController(InstructorService instructorService) {
        this.instructorService = instructorService;
    }
}
