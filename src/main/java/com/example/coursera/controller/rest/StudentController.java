package com.example.coursera.controller.rest;

import com.example.coursera.services.contracts.StudentService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }
}
