package com.example.coursera.services.implementations;

import com.example.coursera.repositories.CourseRepository;
import com.example.coursera.services.contracts.CourseService;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }
}
