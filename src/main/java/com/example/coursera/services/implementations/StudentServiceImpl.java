package com.example.coursera.services.implementations;

import com.example.coursera.repositories.StudentRepository;
import com.example.coursera.services.contracts.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
}
