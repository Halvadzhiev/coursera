package com.example.coursera.services.implementations;

import com.example.coursera.repositories.InstructorRepository;
import com.example.coursera.services.contracts.InstructorService;
import org.springframework.stereotype.Service;

@Service
public class InstructorServiceImpl implements InstructorService {

    private final InstructorRepository instructorRepository;

    public InstructorServiceImpl(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }
}
